.. Sphinx Example Project documentation master file, created by
   sphinx-quickstart on Tue Oct 25 09:18:20 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Testing sphinx and gitlab pages
==================================================

.. toctree::
   :maxdepth: 2
   :caption: Introduction
   
   algorithms/bashexample.rst

Test examples
*************

.. tabs::

   .. code-tab:: c++

         int main(const int argc, const char **argv) {
           return 0;
         }
   .. code-tab:: py

         def main():
             return

   .. code-tab:: java

         class Main {
             public static void main(String[] args) {
             }
         }


Test
****

Is it still working?
